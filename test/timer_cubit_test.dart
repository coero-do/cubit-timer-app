import 'package:bloc_test/bloc_test.dart';
import 'package:cubit_timer_app/app.dart';
import 'package:cubit_timer_app/timer/timer_cubit.dart';
import 'package:cubit_timer_app/timer/timer_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:provider/provider.dart';

class MockTimerCubit extends MockCubit<TimerState> implements TimerCubit {}

class MockTimePicker implements TimePicker {
  @override
  void pickTime(
      final BuildContext context, final void Function(int p1) callback) {
    callback(5000);
  }
}

void main() {
  late TimerCubit timerCubit;
  late MockTimerCubit mockTimerCubit;
  late MaterialApp mockTimerPage;

  final playButton = find.byKey(const ValueKey('playButton'));
  final pauseButton = find.byKey(const ValueKey('pauseButton'));
  final resetButton = find.byKey(const ValueKey('resetButton'));
  final openTimePickerButton =
      find.byKey(const ValueKey('openTimePickerButton'));
  final closeTimePickerButton =
      find.byKey(const ValueKey('closeTimePickerButton'));
  final timePickerPopup = find.byKey(const ValueKey('timePickerPopup'));

  setUp(() {
    timerCubit = TimerCubit();

    mockTimerCubit = MockTimerCubit();
    mockTimerPage = MaterialApp(
        home: BlocProvider<TimerCubit>(
      create: (_) => mockTimerCubit,
      child: const TimerPage(),
    ));

    when(() => mockTimerCubit.state).thenReturn(TimerState(
        durationMillis: 0, type: TimerStateType.initial, selectedTime: 0));
  });
  group('Timer State', () {
    late TimerState state;

    setUp(() {
      state = TimerState(
          durationMillis: 0, type: TimerStateType.initial, selectedTime: 0);
    });

    test("should have initial value of duration millis state", () {
      expect(state.durationMillis, 0);
    });

    test('should have initial value of timer state type', () {
      expect(state.type, TimerStateType.initial);
    });

    test('should have initial value of selected time', () {
      expect(state.selectedTime, 0);
    });
  });

  group('Widget in Timer Cubit', () {
    testWidgets('should render counter', (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 2000, type: TimerStateType.initial, selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);

      expect(find.text('00:02'), findsOneWidget);
    });

    testWidgets('should set time', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider<TimerCubit>(
            create: (_) => mockTimerCubit,
            child: Provider<TimePicker>(
              create: (_) => MockTimePicker(),
              child: const TimerPage(),
            ),
          ),
        ),
      );

      await tester.tap(openTimePickerButton);

      verify(() => mockTimerCubit.setNewTime(5000));
    });

    testWidgets('should open and close pick timer popup',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider<TimerCubit>(
            create: (_) => mockTimerCubit,
            child: Provider<TimePicker>(
              create: (_) => DialogTimePicker(),
              child: const TimerPage(),
            ),
          ),
        ),
      );

      await tester.tap(openTimePickerButton);
      await tester.pump();

      expect(timePickerPopup, findsOneWidget);

      await tester.tap(closeTimePickerButton);
      await tester.pump();

      expect(timePickerPopup, findsNothing);
    });

    testWidgets('should start the timer app', (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 5000, type: TimerStateType.initial, selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);
      await tester.tap(playButton);

      verify(() => mockTimerCubit.start());
    });

    testWidgets('should stop the timer app', (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 5000, type: TimerStateType.running, selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);
      await tester.tap(pauseButton);

      verify(() => mockTimerCubit.pause());
    });

    testWidgets('should reset timer while running',
        (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 5000, type: TimerStateType.running, selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);
      await tester.tap(resetButton);

      verify(() => mockTimerCubit.reset());
    });

    testWidgets('should start timer in paused state',
        (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 5000, type: TimerStateType.paused, selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);
      await tester.tap(playButton);

      verify(() => mockTimerCubit.start());
    });

    testWidgets('should reset timer in paused state',
        (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 5000, type: TimerStateType.paused, selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);
      await tester.tap(resetButton);

      verify(() => mockTimerCubit.reset());
    });

    testWidgets('should reset timer in completed state',
        (WidgetTester tester) async {
      when(() => mockTimerCubit.state).thenReturn(TimerState(
          durationMillis: 5000,
          type: TimerStateType.completed,
          selectedTime: 0));

      await tester.pumpWidget(mockTimerPage);
      await tester.tap(resetButton);

      verify(() => mockTimerCubit.reset());
    });

    testWidgets('should initial not show pause and reset button',
        (WidgetTester tester) async {
      final resetButton = find.byKey(const ValueKey('resetButton'));
      final pauseButton = find.byKey(const ValueKey('pauseButton'));

      await tester.pumpWidget(const App());

      expect(resetButton, findsNothing);
      expect(pauseButton, findsNothing);
    });
  });

  group('Timer Cubit class', () {
    blocTest('should emit start method',
        build: () => timerCubit,
        act: (TimerCubit cubit) => cubit.start(),
        expect: () => [isA<TimerState>()]);

    blocTest('should emit pause method',
        build: () => timerCubit,
        act: (TimerCubit cubit) => cubit.pause(),
        expect: () => [isA<TimerState>()]);

    blocTest('should emit reset method',
        build: () => timerCubit,
        act: (TimerCubit cubit) => cubit.reset(),
        expect: () => [isA<TimerState>()]);

    blocTest('should emit setNewTime method',
        build: () => timerCubit,
        act: (TimerCubit cubit) => cubit.setNewTime(5000),
        expect: () => [isA<TimerState>()]);
  });
}

import 'package:bloc/bloc.dart';
import 'dart:async';

enum TimerStateType {
  initial,
  paused,
  running,
  completed,
}

class TimerState {
  final int durationMillis;
  final TimerStateType type;
  final int selectedTime;

  TimerState({
    required this.durationMillis,
    required this.type,
    required this.selectedTime,
  });
}

class TimerCubit extends Cubit<TimerState> {
  static final _initialState = TimerState(
      durationMillis: 0, type: TimerStateType.initial, selectedTime: 0);

  TimerCubit() : super(_initialState);

  void start() {
    emit(TimerState(
      durationMillis: state.durationMillis,
      type: TimerStateType.running,
      selectedTime: state.selectedTime,
    ));
    _countdown();
  }

  void pause() {
    emit(TimerState(
      durationMillis: state.durationMillis,
      type: TimerStateType.paused,
      selectedTime: state.selectedTime,
    ));
  }

  void reset() => emit(TimerState(
        durationMillis: state.selectedTime,
        type: TimerStateType.initial,
        selectedTime: state.selectedTime,
      ));

  void setNewTime(int time) {
    emit(TimerState(
      durationMillis: time,
      type: TimerStateType.initial,
      selectedTime: time,
    ));
  }

  void _countdown() {
    const intervalMillis = 100;
    Timer.periodic(const Duration(milliseconds: intervalMillis), (timer) {
      final isCompleted = state.durationMillis == 0;

      if (state.type == TimerStateType.running && !isCompleted) {
        emit(TimerState(
          durationMillis: state.durationMillis - intervalMillis,
          type: TimerStateType.running,
          selectedTime: state.selectedTime,
        ));
        return;
      }

      timer.cancel();

      if (isCompleted) {
        emit(TimerState(
          durationMillis: state.durationMillis,
          type: TimerStateType.completed,
          selectedTime: state.selectedTime,
        ));
      }
    });
  }
}

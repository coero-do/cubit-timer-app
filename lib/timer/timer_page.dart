import 'package:cubit_timer_app/app.dart';
import 'package:cubit_timer_app/timer/timer_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TimerPage extends StatelessWidget {
  const TimerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cubit counter App'),
      ),
      body: Stack(children: [
        const _Background(),
        BlocBuilder<TimerCubit, TimerState>(builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 100.0),
                child: Center(
                  child: Column(
                    children: [
                      _Text(state: state),
                      const SizedBox(
                        height: 40,
                      ),
                      _TimePicker(state: state),
                    ],
                  ),
                ),
              ),
              _Actions(state: state),
            ],
          );
        }),
      ]),
    );
  }
}

class _TimePicker extends StatelessWidget {
  final TimerState state;

  const _TimePicker({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        key: const Key('openTimePickerButton'),
        onPressed: () {
          context.read<TimePicker>().pickTime(
              context, (value) => context.read<TimerCubit>().setNewTime(value));
        },
        child: const Text('Pick time'),
      ),
    );
  }
}

class _Text extends StatelessWidget {
  const _Text({Key? key, required this.state}) : super(key: key);

  final TimerState state;

  @override
  Widget build(BuildContext context) {
    final duration = state.durationMillis / 1000;

    format(double n) => n.floor().toString().padLeft(2, '0');
    final minutes = format((duration / 60) % 60);
    final seconds = format(duration % 60);

    return Text(
      key: const Key('counter'),
      '$minutes:$seconds',
      style: Theme.of(context).textTheme.headline1,
    );
  }
}

class _Actions extends StatelessWidget {
  final TimerState state;

  const _Actions({
    Key? key,
    required this.state,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: _buildActions(context),
      );

  Widget _buildActions(BuildContext context) {
    final timer = context.read<TimerCubit>();

    switch (state.type) {
      case TimerStateType.initial:
        return _buildPlayButton(context, timer);

      case TimerStateType.paused:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildPlayButton(context, timer),
            _buildResetButton(context, timer),
          ],
        );

      case TimerStateType.running:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildPauseButton(context, timer),
            _buildResetButton(context, timer),
          ],
        );

      case TimerStateType.completed:
        return _buildResetButton(context, timer);
    }
  }

  Widget _buildResetButton(
    BuildContext context,
    TimerCubit timer,
  ) {
    return FloatingActionButton(
      key: const Key('resetButton'),
      onPressed: () => timer.reset(),
      tooltip: 'Reset',
      child: const Icon(Icons.replay),
    );
  }

  Widget _buildPauseButton(
    BuildContext context,
    TimerCubit timer,
  ) {
    return FloatingActionButton(
      key: const Key('pauseButton'),
      onPressed: () => timer.pause(),
      tooltip: 'Pause',
      child: const Icon(Icons.pause),
    );
  }

  Widget _buildPlayButton(
    BuildContext context,
    TimerCubit timer,
  ) {
    return FloatingActionButton(
      key: const Key('playButton'),
      onPressed: () => timer.start(),
      tooltip: 'Play',
      child: const Icon(Icons.play_arrow),
    );
  }
}

class _Background extends StatelessWidget {
  const _Background({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.blue.shade50,
            Colors.blue.shade500,
          ],
        ),
      ),
    );
  }
}

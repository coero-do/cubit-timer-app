import 'package:cubit_timer_app/timer/timer_page.dart';
import 'package:cubit_timer_app/timer/timer_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TimerCubit>(
      create: (BuildContext context) => TimerCubit(),
      child: MaterialApp(
        title: 'Flutter Cubit Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Provider<TimePicker>(
          create: (_) => DialogTimePicker(),
          child: const TimerPage(),
        ),
      ),
    );
  }
}

abstract class TimePicker {
  void pickTime(BuildContext context, void Function(int) callback);
}

class DialogTimePicker implements TimePicker {
  @override
  void pickTime(final BuildContext context, void Function(int) callback) {
    int selectedTime = 0;

    showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        key: const Key('timePickerPopup'),
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Center(
                child: SizedBox(
                  height: 200,
                  child: CupertinoTimerPicker(
                    mode: CupertinoTimerPickerMode.ms,
                    onTimerDurationChanged: (time) =>
                        selectedTime = time.inMilliseconds.toInt(),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    key: const Key('closeTimePickerButton'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Close Time Picker'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      callback(selectedTime);
                      Navigator.pop(context);
                    },
                    child: const Text('Apply'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
